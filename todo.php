<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-5.2.3-dist/css/bootstrap.min.css">
    <title>Add to Todo List</title>
    <style>
        header{
            background-color: black;
            color: white;
            margin: 0;
            padding: 6px;
        }
        h1{
            padding-left: 105px;
        }
        #title{
            width: 42%;
            display: inline;
        }
        .list{
            padding-left: 40px;
        }
        .t1{
            margin: 10px;
        }
        .bg-success,.bg-danger{
            --bs-bg-opacity: 1;
    width: 222px;
    color: white;
    border-radius: 7px;
    height: 50px;
    padding-top: 10px;
    /* margin-top: 46px; */
    /* margin: 0px; */
    padding-left: 35px;
        }
        .btn1 {
    margin-left: 226px;
    margin-top: -23px;
    width: 81px;
    height: 38px;
        }

    </style>
</head>
<body>
<header>
    <h1>Todo List</h1>
</header>
<br><br>
<center><form action="todo.php" method="post">

    <input type="text" id="title" name="title" placeholder="title" class="form-control">

    <input type="submit" value="Add" class="btn btn-primary">
</form></center>
<br><br><br>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "todolist";  

$conn = new mysqli($servername, $username, $password, $dbname);


$result = $conn->query("SELECT id, title, done, created_at FROM todo");

echo "<div class='list'>";

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $id = $row["id"];
        $title = htmlspecialchars($row["title"]);
        $done = $row["done"];

        $backgroundColor = ($done == 0) ? 'bg-danger' : 'bg-success';

        echo "<div class='rounded p-2 $backgroundColor'>$title";
        echo "<form class='d-inline' method='post' action='todo.php'>";
        echo "<input type='hidden' name='id' value='$id'>";
        echo "<div class='btn1'><button type='submit' class='btn btn-danger btn-sm ms-2'>x</button>";
        echo "<button type='submit' class='btn btn-primary btn-sm ms-2' name='undo'>Undo</button></div>";
        echo "</form></div><br>";
    }
}

echo "</div>";

//-----------add-------------

if(isset($_POST['title'])){
    if(!empty($_POST['title'])){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $title = $_POST["title"];
            $sql = "INSERT INTO todo (title) VALUES ('$title')";
        
            if ($conn->query($sql) === TRUE) {
                echo "New record added successfully";
            } 
        }        
    }
};
//---------------delete,update----------------


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id'])) {
    $id = $_POST['id'];

    $sql = "DELETE FROM todo WHERE id = $id";

    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } 
}

$conn->close();
?>
?>

</body>
</html>

